﻿namespace Practice.DataStructures.BinaryTreeDS
{
    public class BinaryTree
    {
        private Node top;

        public BinaryTree()
        {
            top = null;
        }

        public BinaryTree(int initial)
        {
            top = new Node(initial);
        }

        public void Add(int value)
        {
            // non-recursive add
            if (top == null) // The tree is empty
            {
                // Add item as base node
                Node NewNode = new Node(value);
                top = NewNode;
                return;
            }

            Node currentNode = top;
            bool added = false;

            do
            {
                //traverse tree
                if (value < currentNode.value)
                {
                    if (currentNode.left == null)
                    {
                        Node NewNode = new Node(value);
                        currentNode.left = NewNode;
                        added = true;
                    }
                    else
                    {
                        currentNode = currentNode.left;
                    }
                }
                if (value >= currentNode.value)
                {
                    if (currentNode.right == null)
                    {
                        Node NewNode = new Node(value);
                        currentNode.right = NewNode;
                        added = true;
                    }
                    else
                    {
                        currentNode = currentNode.right;
                    }
                }
            } while (!added);
        }

        public void AddRc(int value)
        {
            // recursive add
            AddR(ref top, value);
        }

        private void AddR(ref Node N, int value)
        {
            //private recurssive search for where to add the new node
            if (N == null)
            {
                //Node doesn't exist, add it here
                Node NewNode = new Node(value);
                N = NewNode; //Set the old Node reference to the newly created node thus attaching it to the tree
                return; //End the function call and fall back
            }
            if (value < N.value)
            {
                AddR(ref N.left, value);
                return;
            }
            if (value >= N.value)
            {
                AddR(ref N.right, value);
                return;
            }
        }

        public void Print(Node N, ref string s)
        {
            //Write out the tree in sorted order to the string newstring
            //implement using recursion
            if (N == null) { N = top; }
            if (N.left != null)
            {
                Print(N.left, ref s);
                s = s + N.value.ToString().PadLeft(3);
            }
            else
            {
                s = s + N.value.ToString().PadLeft(3);
            }
            if (N.right != null)
            {
                Print(N.right, ref s);
            }
        }
    }
}