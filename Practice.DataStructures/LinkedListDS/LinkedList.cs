﻿using System;

namespace Practice.DataStructures.LinkedListDS
{
    public class LinkedList
    {
        /* Constructor
         * [x] LinkedList() - Initializes the private fields
         *
         * Private Fields:
         * [x] Node head - Isa reference to the FIRST node in the list
         * [x] int size - The currentsize of the list
         *
         * Public Properties:
         * [x] Empty - If the list is empty
         * [x] Count - How many items are in the list
         * [x] Indexer - Access the data like an array
         *
         * Methods:
         * [x] Add(int index, object o) - Add an item to list at specified index
         * [x] Add(object o) - Add an item to the END of the list
         * [x] Remove(int index)- Removes the item in the list at the specified index
         * [x] Clear() - Clear the list
         * [x] IndexOf(object o) - if the list contains the object
         * [x] Get(int index) - Gets item at the specified index
         *
         */

        private Node head;
        private int count;

        public LinkedList()
        {
            this.head = null;
            this.count = 0;
        }

        public bool Empty
        {
            get { return this.count == 0; }
        }

        public int Count
        {
            get { return this.count; }
        }

        public object Add(int index, object o)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (index > count)
                index = count;

            Node current = this.head;

            if (this.Empty || index == 0)
            {
                this.head = new Node(o, this.head);
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                    current = current.Next;

                current.Next = new Node(o, current.Next);
            }

            count++;

            return o;
        }

        public object Add(object o)
        {
            return this.Add(count, o);
        }

        public object Remove(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (this.Empty)
                return null;

            if (index >= this.count)
                index = count - 1;

            Node current = this.head;
            object result = null;

            if (index == 0)
            {
                result = current.Data;
                this.head = current.Next;
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                    current = current.Next;

                result = current.Next.Data;
                current.Next = current.Next.Next;
            }

            count--;
            return result;
        }

        public void Clear()
        {
            this.head = null;
            this.count = 0;
        }

        public int IndexOf(object o)
        {
            Node current = this.head;
            for (int i = 0; i < this.count; i++)
            {
                if (current.Data.Equals(o))
                    return i;

                current = current.Next;
            }

            return -1;
        }

        public bool Contains(object o)
        {
            return this.IndexOf(o) > -1;
        }

        public object this[int index]
        {
            get { return this.Get(index); }
        }

        public object Get(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Index: " + index);

            if (this.Empty)
                return null;

            if (index >= this.count)
                index = this.count - 1;

            Node current = this.head;

            for (int i = 0; i < index; i++)
                current = current.Next;

            return current.Data;
        }
    }
}