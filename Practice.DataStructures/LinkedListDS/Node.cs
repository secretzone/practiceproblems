﻿namespace Practice.DataStructures.LinkedListDS
{
    public class Node
    {
        /* Constructor
         * [x] Node(object data, Node next)
         *
         * Private Fields:
         * [x] object data - contains the data ofthe node, what we want to store in thelist
         * [x] Node next - reference to the next node in the list
         *
         * Public Properties:
         * [x] Data - get/set thedata field
         * [x] Next - get set the next field
         */

        private object data;
        private Node next;

        public Node(object data, Node next)
        {
            this.data = data;
            this.next = next;
        }

        public object Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        public Node Next
        {
            get { return this.next; }
            set { this.next = value; }
        }
    }
}