﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Practice.Methods
{
    public class AddsUpTo
    {
        public static List<int[]> AddsUpToInt(int[] intArray, int compareInt)
        {
            int count = intArray.Count();
            List<int[]> result = new List<int[]>();
            for (int i = 0; i < intArray.Count(); i++)
                for (int k = i + 1; k < intArray.Count(); k++)
                {
                    if (i == k) //so as not to compare an element with itself, but this should never happen
                    {
                        //string t = string.Format("DUPLICATE: {0} + {1} = {2}", intArray[i], intArray[k], compareInt);
                        //Debug.WriteLine(t);
                        continue;
                    }

                    int sum = intArray[i] + intArray[k];

                    if (sum == compareInt)
                    {
                        string t = string.Format("EQUAL: {0} + {1} = {2}", intArray[i], intArray[k], compareInt);
                        int[] n = { intArray[i], intArray[k] };
                        result.Add(n);
                        Debug.WriteLine(t);
                    }
                    else
                    {
                        //string t = string.Format("UNEQUAL: {0} + {1} != {2}", intArray[i], intArray[k], compareInt);
                        //Debug.WriteLine(t);
                    }
                }
            //string[] temp = { "5 + 10 = 15" };
            //return temp;

            return result;
        }
    }
}