﻿using System.Linq;

namespace Practice.Methods
{
    public static class AreTheySame
    {
        /// <summary>
        /// IN PROGRESS
        /// Still fails advanced tests
        ///
        /// Takes two arrays of numbers and checks whether
        /// the two arrays have the "same" elements, with
        /// the same multiplicities. "Same" means, here,
        /// that the elements in b are the elements in a
        /// squared, regardless of the order.
        ///
        /// https://www.codewars.com/kata/are-they-the-same/train/csharp
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Compare(int[] a, int[] b)
        {
            if (a.Count() != b.Count() || a.Count() == 0 || b.Count() == 0)
                return false;
            foreach (int i in a)
            {
                if (!(b.Contains(i * i)))
                    return false;
            }
            return true;
        }
    }
}