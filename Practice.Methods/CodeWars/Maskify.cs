﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.Methods.CodeWars
{
    public class Maskify
    {
        /// <summary>
        /// Masks all but last 4 characters of a string
        /// 
        /// From unit tests, using anonymous methods
        /// </summary>
        /// <param name="cc"></param>
        /// <returns></returns>
        public static string MaskifyString3(string cc)
        {
            return cc.Length <= 4 ? cc : new String('#', cc.Length - 4) + cc.Substring(cc.Length - 4);
        }

        /// <summary>
        /// Masks all but last 4 characters of a string
        /// 
        /// My initial solution
        /// </summary>
        /// <param name="cc"></param>
        /// <returns></returns>
        public static string MaskifyString(string cc)
        {
            char[] splitstr = cc.ToArray();
            string result = "";
            int len = splitstr.Length;
            if (len < 4)
                return cc;

            for( int i = 0; i < len; i++)
                if(i >= len - 4)
                {
                    result += splitstr[i];
                }
                else { result += "#"; }

            return result;
        }

        /// <summary>
        /// Masks all but last 4 characters of a string.
        /// 
        /// Best practices answer
        /// </summary>
        /// <param name="cc"></param>
        /// <returns></returns>
        public static string MaskifyString2(string cc)
        {
            int len = cc.Length;
            if (len <= 4)
                return cc;

            return cc.Substring(len - 4).PadLeft(len, '#');
        }
    }
}
