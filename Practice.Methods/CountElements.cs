﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Practice.Methods
{
    public class CountElements
    {
        /// <summary>
        /// Converts to binary then counts the '1's
        ///
        /// Passes all tests
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static int CountBits(int n)
        {
            //convert int to base 2 as string, then count '1's
            return Convert.ToString(n, 2).Count(c => c == '1');
        }

        public static int CountChars(string str, char letter)
        {
            int count = 0;
            //char[] splitstring = str.ToCharArray();
            foreach (char c in str)
            {
                if (c == letter)
                {
                    count++;
                }
            }

            return count;
        }

        public static int CountSubstrings(string str, string substr)
        {
            return Regex.Matches(str, substr).Count;
        }
    }
}