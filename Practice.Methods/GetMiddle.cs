﻿namespace Practice.Methods
{
    /// <summary>
    /// https://www.codewars.com/kata/get-the-middle-character/csharp
    /// </summary>
    public class GetMiddle
    {


        /// <summary>
        /// Get middle of a string
        /// 
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetMiddleOfString(string s)
        {
            if (s.Length == 0)
                return "";
            string result = "";
            int len = s.Length;
            if (len % 2 == 0)
                result = s.Substring(((len / 2) - 1), 2);
            else
                result = s[len / 2].ToString();

            return result;
        }

        /// <summary>
        /// Get middle of a string
        /// 
        /// Best practices answer
        /// biskinis, XoroshiStudent
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetMiddleOfString2(string s)
        {
            return string.IsNullOrEmpty(s)
                ? s
                : s.Substring((s.Length - 1) / 2, 2 - s.Length % 2);
        }
    }
}