﻿using System.Linq;

namespace Practice.Methods.InterviewQuestions
{
    public class ReverseString
    {
        public static string Reverse(string str)
        {
            string result = "";
            char[] strArray = str.ToArray();

            for (int i = str.Length - 1; i >= 0; i--)
                result += strArray[i];

            return result;
        }
    }
}