﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace Practice.Methods
{
    public static class JadenCase
    {
        /// <summary>
        /// Convert strings to how they would be written by Jaden Smith.
        /// In other words, capitalizes the first letter of every word
        /// in a given string.
        ///
        ///
        /// </summary>
        /// <param name="phrase"></param>
        /// <returns></returns>
        public static string ToJadenCase(this string phrase)
        {
            if (string.IsNullOrEmpty(phrase))
            {
                return string.Empty;
            }
            return Regex.Replace(phrase, @"(^\w)|(\s\w)", m => m.Value.ToUpper());
        }

        /// <summary>
        /// Convert strings to how they would be written by Jaden Smith.
        /// In other words, capitalizes the first letter of every word
        /// in a given string.
        ///
        /// Best Practices answer.
        /// Credit: iSpender, Dentzil, hicksj
        /// </summary>
        /// <param name="phrase"></param>
        /// <returns></returns>
        public static string ToJadenCase2(this string phrase)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(phrase);
        }
    }
}