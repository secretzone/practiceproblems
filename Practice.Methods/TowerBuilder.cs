﻿using System.Diagnostics;

namespace Practice.Methods
{
    public static class TowerBuilder
    {
        // My answer
        /// <summary>
        /// Builds a tower n floors high
        ///
        ///       [
        ///'     *     ',    5 1 5   = 11
        ///'    ***    ',    4 3 4   = 11
        ///'   *****   ',    3 5 3   = 11
        ///'  *******  ',    2 7 2   = 11
        ///' ********* ',    1 9 1   = 11
        ///'***********'     0 11 0  = 11
        ///]
        ///
        /// https://www.codewars.com/kata/576757b1df89ecf5bd00073b
        /// </summary>
        /// <param name="nFloors"></param>
        /// <returns></returns>
        public static string[] BuildTower(int nFloors)
        {
            string[] floorlist = new string[nFloors];
            int c = 1;
            int s = nFloors - 1;
            for (int floor = 0; floor < nFloors; floor++)
            {
                floorlist[floor] = new string(' ', s - floor);
                for (int i = 0; i < c; i++)
                {
                    floorlist[floor] += "*";
                }
                floorlist[floor] += new string(' ', s - floor); //learned this is a thing

                c += 2;

                Debug.WriteLine(floorlist[floor]);
            }

            return floorlist;
        }

        // Best Practices answer. PouyaBuddy
        public static string[] TowerBuilder2(int nFloors)
        {
            var result = new string[nFloors];
            for (int i = 0; i < nFloors; i++)
                result[i] = string.Concat(new string(' ', nFloors - i - 1),
                                          new string('*', i * 2 + 1),
                                          new string(' ', nFloors - i - 1));
            return result;
        }
    }
}