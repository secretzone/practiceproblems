﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Practice.Methods
{
    public class WhichAreIn
    {
        /// <summary>
        /// Takes two arrays (a1, a2) and returns a sorted array in
        /// lexicographical order of the strings of a1 which are
        /// substrings of strings of a2.
        ///
        /// Fails test if Assert is used instead of CollectionAssert
        ///
        /// https://www.codewars.com/kata/550554fd08b86f84fe000a58
        /// </summary>
        /// <param name="array1"></param>
        /// <param name="array2"></param>
        /// <returns></returns>
        public static string[] InArray(string[] array1, string[] array2)
        {
            // your code
            List<string> result = new List<string>();
            foreach (string s1 in array1)
            {
                foreach (string s2 in array2)
                {
                    if (s2.Contains(s1))
                    {
                        result.Add(s1);
                        break;
                    }
                }
            }

            string[] temp = result.ToArray();

            foreach (var item in temp)
            {
                Debug.WriteLine(item.ToString());
            }

            return temp;
        }
    }
}