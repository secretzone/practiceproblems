﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Practice.Methods.Tests
{
    [TestClass()]
    public class AddsUpToTests
    {
        private static TestContext testContext;
        private static Random randomNumber;

        [ClassInitialize()]
        public static void Initialize(TestContext testContext)
        {
            randomNumber = new Random();
        }

        [ClassCleanup()]
        public static void Cleanup()
        {
            randomNumber = null;
        }

        [TestMethod()]
        public void AddsUpToIntTest()
        {
            int[] inputArray = { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            int checkValue = 15;

            List<int[]> actual = AddsUpTo.AddsUpToInt(inputArray, checkValue);
            foreach (int[] iArray in actual)
            {
                if (iArray[0] + iArray[1] != checkValue)//Check if any numbers don't add up properly
                {
                    Assert.Fail();
                }
            }
            Assert.IsTrue(true); //If we get this far we succeeded
        }

        [TestMethod()]
        public void AddsUpToInt_Random_Test()
        {
            for (int c = 0; c < 100; c++)
            {
                int Min = randomNumber.Next(0, 100);
                int Max = randomNumber.Next(Min + 1, Min + 1000);
                //randomNumber = new Random();
                int[] inputArray = Enumerable
                    .Repeat(0, 99)
                    .Select(i => randomNumber.Next(Min, Max))
                    .ToArray();
                int checkValue = randomNumber.Next(Min, Max * 2);
                string sArray = "";
                bool first = true;
                foreach (var v in inputArray)
                {
                    if (first)
                    {
                        sArray += v;
                        first = false;
                    }
                    else
                    {
                        sArray += "," + v;
                    }
                }
                Debug.WriteLine("Min: {0} Max: {1} Seeking a pair with sum: {2} From List: [{3}]", Min, Max, checkValue, sArray);
                List<int[]> actual = AddsUpTo.AddsUpToInt(inputArray, checkValue);
                foreach (int[] iArray in actual)
                {
                    if (iArray[0] + iArray[1] != checkValue)
                    {
                        string s = string.Format("{0} + {1} != {2}", iArray[0], iArray[1], checkValue);
                        Assert.Fail(s);
                    }
                }
                Assert.IsTrue(true);
            }
        }
    }
}