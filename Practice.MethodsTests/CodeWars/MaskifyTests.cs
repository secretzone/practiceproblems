﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Practice.Methods.CodeWars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.Methods.CodeWars.Tests
{
    /// <summary>
    /// https://www.codewars.com/kata/5412509bd436bd33920011bc/solutions/csharp
    /// </summary>
    [TestClass()]
    public class MaskifyTests
    {
        #region MaskifyString1
        [TestMethod()]
        public void ExamplesTests()
        {
            Assert.AreEqual(Maskify.MaskifyString("4556364607935616"), "############5616");
            Assert.AreEqual(Maskify.MaskifyString("1"), "1");
            Assert.AreEqual(Maskify.MaskifyString("11111"), "#1111");
        }

        [TestMethod()]
        public void RandomTests()
        {
            Func<string, string> solution = (cc) =>
            {
                return cc.Length <= 4 ? cc : new String('#', cc.Length - 4) + cc.Substring(cc.Length - 4);
            };
            Random rand = new Random();
            Func<string> randomToken = () =>
            {
                return rand.Next(1000, 9999).ToString();
            };

            for (int i = 0; i < 100; i++)
            {   //   XXXXXXXXXXXXXXXX        1000-9000   +    1000-9000  +  1000-9000    +   1000-9000
                string t = randomToken() + randomToken() + randomToken() + randomToken();
                t = t.Substring(0, 1 + (rand.Next(1, 15) % t.Length));
                Assert.AreEqual(Maskify.MaskifyString(t), solution(t));
            }
        }
        #endregion
        #region MaskifyString2

        [TestMethod()]
        public void ExamplesTests2()
        {
            Assert.AreEqual(Maskify.MaskifyString2("4556364607935616"), "############5616");
            Assert.AreEqual(Maskify.MaskifyString2("1"), "1");
            Assert.AreEqual(Maskify.MaskifyString2("11111"), "#1111");
        }

        [TestMethod()]
        public void RandomTests2()
        {
            Func<string, string> solution = (cc) =>
            {
                return cc.Length <= 4 ? cc : new String('#', cc.Length - 4) + cc.Substring(cc.Length - 4);
            };
            Random rand = new Random();
            Func<string> randomToken = () =>
            {
                return rand.Next(1000, 9999).ToString();
            };

            for (int i = 0; i < 100; i++)
            {   //   XXXXXXXXXXXXXXXX        1000-9000   +    1000-9000  +  1000-9000    +   1000-9000
                string t = randomToken() + randomToken() + randomToken() + randomToken();
                t = t.Substring(0, 1 + (rand.Next(1, 15) % t.Length));
                Assert.AreEqual(Maskify.MaskifyString2(t), solution(t));
            }
        }
        #endregion
        #region MaskifyString3

        [TestMethod()]
        public void ExamplesTests3()
        {
            Assert.AreEqual(Maskify.MaskifyString3("4556364607935616"), "############5616");
            Assert.AreEqual(Maskify.MaskifyString3("1"), "1");
            Assert.AreEqual(Maskify.MaskifyString3("11111"), "#1111");
        }

        [TestMethod()]
        public void RandomTests3()
        {
            Func<string, string> solution = (cc) =>
            {
                return cc.Length <= 4 ? cc : new String('#', cc.Length - 4) + cc.Substring(cc.Length - 4);
            };
            Random rand = new Random();
            Func<string> randomToken = () =>
            {
                return rand.Next(1000, 9999).ToString();
            };

            for (int i = 0; i < 100; i++)
            {   //   XXXXXXXXXXXXXXXX     1000-9000 + 1000-9000 + 1000-9000 + 1000-9000
                string t = randomToken() + randomToken() + randomToken() + randomToken();
                t = t.Substring(0, 1 + (rand.Next(1, 15) % t.Length));
                Assert.AreEqual(Maskify.MaskifyString3(t), solution(t));
            }
        }
    }
    #endregion
}