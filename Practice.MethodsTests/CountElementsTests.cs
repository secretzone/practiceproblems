﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Practice.Methods.Tests
{
    [TestClass()]
    public class CountElementsTests
    {
        [TestMethod()]
        public void CountBitsTest()
        {
            Assert.AreEqual(0, CountElements.CountBits(0));
            Assert.AreEqual(1, CountElements.CountBits(4));
            Assert.AreEqual(3, CountElements.CountBits(7));
            Assert.AreEqual(2, CountElements.CountBits(9));
            Assert.AreEqual(2, CountElements.CountBits(10));
        }

        [TestMethod()]
        public void CountCharsTest()
        {
            char letter = 'a';
            string str = "all your base are belong to us";
            int expected = 3;

            Assert.AreEqual(expected, CountElements.CountChars(str, letter));
        }

        [TestMethod()]
        public void CountChars_Empty_Test()
        {
            char letter = 'a';
            string str = "";
            int expected = 0;

            Assert.AreEqual(expected, CountElements.CountChars(str, letter));
        }

        [TestMethod()]
        public void CountSubstringsTest()
        {
            int expected = 4;
            string str = "dogs, doggos, dog, diggitydog, do go, d'og";
            string substr = "dog";

            Assert.AreEqual(expected, CountElements.CountSubstrings(str, substr));
        }
    }
}