﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Practice.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice.Methods.Tests
{
    [TestClass()]
    public class GetMiddleTests
    {
        [TestMethod()]
        public void GetMiddleOfStringTest()
        {
            Assert.AreEqual("es", GetMiddle.GetMiddleOfString("test"));
            Assert.AreEqual("t", GetMiddle.GetMiddleOfString("testing"));
            Assert.AreEqual("dd", GetMiddle.GetMiddleOfString("middle"));
            Assert.AreEqual("A", GetMiddle.GetMiddleOfString("A"));
        }

        //-------------------
        
        public string GetMiddleT(string s)
        {
            int middle = s.Length / 2;
            if (s.Length % 2 == 0)
                return s[middle - 1].ToString() + s[middle].ToString();
            else
                return s[middle].ToString();
        }
        //-------------------

        [TestMethod()]
        public void RandomStringTests()
        {
            Console.WriteLine("\n ********** 50 Random String Tests **********");
            string alph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random rnd = new Random();
            for (int i = 0; i < 50; i++)
            {
                string randomString = "";
                int rando = rnd.Next(1, 1000);
                for (int j = 0; j < rando; j++)
                {
                    int n = rnd.Next(0, alph.Length);
                    randomString += alph[n];
                }
                Assert.AreEqual(GetMiddleT(randomString), GetMiddle.GetMiddleOfString(randomString));
            }
        }
    }
}