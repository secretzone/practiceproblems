﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Practice.Methods.InterviewQuestions.Tests
{
    [TestClass()]
    public class ReverseStringTests
    {
        [TestMethod()]
        public void ReverseTest()
        {
            string input = "abcdefghijklmnopqrstuvwxyz";
            string expected = "zyxwvutsrqponmlkjihgfedcba";

            Assert.AreEqual(expected, ReverseString.Reverse(input));
        }
    }
}