﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Practice.Methods.Tests
{
    [TestClass()]
    public class JadenCaseTests
    {
        public TestContext TestContext { get; set; }

        [TestMethod()]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                    "JadenCase.xml",
                    "Phrase", DataAccessMethod.Sequential)]
        public void ToJadenCaseTest()
        {
            string input = TestContext.DataRow["Input"].ToString();
            string output = TestContext.DataRow["Output"].ToString();
            Assert.AreEqual(JadenCase.ToJadenCase(input), output);
        }

        [TestMethod()]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "JadenCase.xml",
            "Phrase", DataAccessMethod.Sequential)]
        public void ToJadenCaseTest2()
        {
            string input = TestContext.DataRow["Input"].ToString();
            string output = TestContext.DataRow["Output"].ToString();
            Assert.AreEqual(JadenCase.ToJadenCase2(input), output);
        }
    }
}