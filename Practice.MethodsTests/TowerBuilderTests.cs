﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Practice.Methods.Tests
{
    [TestClass()]
    public class TowerBuilderTests
    {
        [TestMethod]
        public void Tower_Test()
        {
            Assert.AreEqual(string.Join(",", new[] { "*" }), string.Join(",", TowerBuilder.BuildTower(1)));
            Assert.AreEqual(string.Join(",", new[] { " * ", "***" }), string.Join(",", TowerBuilder.BuildTower(2)));
            Assert.AreEqual(string.Join(",", new[] { "  *  ", " *** ", "*****" }), string.Join(",", TowerBuilder.BuildTower(3)));
        }

        [TestMethod]
        public void Tower_Test2()
        {
            Assert.AreEqual(string.Join(",", new[] { "*" }), string.Join(",", TowerBuilder.TowerBuilder2(1)));
            Assert.AreEqual(string.Join(",", new[] { " * ", "***" }), string.Join(",", TowerBuilder.TowerBuilder2(2)));
            Assert.AreEqual(string.Join(",", new[] { "  *  ", " *** ", "*****" }), string.Join(",", TowerBuilder.TowerBuilder2(3)));
        }

        [TestMethod]
        public void RandomTests()
        {
            var rand = new Random();

            Func<int, string[]> myTowerBuilder = delegate (int nFloors)
            {
                string[] lines = new string[nFloors];
                for (var i = 1; i <= nFloors; i++)
                {
                    lines[i - 1] = (new string(' ', nFloors - i) + new string('*', i * 2 - 1) + new string(' ', nFloors - i));
                }
                return lines;
            };

            var seq = Enumerable.Range(1, 100).ToArray();
            for (int r = 0; r < 100; r++)
            {
                for (int p = 0; p < 100; p++)
                {
                    if (rand.Next(0, 2) == 0)
                    {
                        var temp = seq[r];
                        seq[r] = seq[p];
                        seq[p] = temp;
                    }
                }
            }

            for (int r = 0; r < 100; r++)
            {
                var n = seq[r];
                //Console.WriteLine(n);
                Assert.AreEqual(string.Join(",", myTowerBuilder(n)), string.Join(",", TowerBuilder.BuildTower(n)));
            }
        }
    }
}