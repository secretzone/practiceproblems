﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Practice.Methods.Tests
{
    [TestClass()]
    public class WhichAreInTests
    {
        [TestMethod]
        public void InArray_Test1()
        {
            string[] a1 = new string[] { "arp", "live", "strong" };
            string[] a2 = new string[] { "lively", "alive", "harp", "sharp", "armstrong" };
            string[] r = new string[] { "arp", "live", "strong" };
            string[] test = WhichAreIn.InArray(a1, a2);
            CollectionAssert.AreEqual(r, test);
            //Using Assert instead will cause the test to fail
        }
    }
}