These are my solutions to various programming challenges in C#, complete with tests, from sources such as CodeWars, etc.

I include my initial solution, plus the best practices solution (giving credit) if available so that I can learn from the process.

